require('dotenv').config()
const express = require('express')
const compression = require('compression')
const session = require('express-session')
const bodyParser = require('body-parser')
const lusca = require('lusca')
const flash = require('express-flash')
const path = require('path')
const expressStatusMonitor = require('express-status-monitor')
const logger = require('./logger')

// const multer = require('multer')
// const upload = multer({ dest: path.join(__dirname, 'uploads') })

const app = express()

app.set('host', process.env.HOST || '127.0.01')
app.set('port', parseInt(process.env.PORT, 10) || 8080)
app.set('views', path.join(__dirname, 'views'))

app.set('view engine', 'pug')

app.use(expressStatusMonitor())
app.use(compression())

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(session({
  resave: true,
  saveUninitialized: true,
  secret: process.env.SESSION_SECRET,
  cookie: { maxAge: 1209600000 } // two weeks in milliseconds
}))
app.use(flash())
app.use((req, res, next) => {
  if (req.path === '/api/upload') {
    // Multer multipart/form-data handling needs to occur before the Lusca CSRF check.
    next()
  } else {
    lusca.csrf()(req, res, next)
  }
})

app.use(lusca.xframe('SAMEORIGIN'))
app.use(lusca.xssProtection(true))
app.disable('x-powered-by')
logger.info(`public path for assets: ${path.join(__dirname, 'public')}`)
app.use(express.static(path.join(__dirname, 'public'), { maxAge: 31557600000 }))

/**
 * Controllers
 */
app.get('/', (req, res) => {
  res.render('home', {
    title: 'Inicio',
    locales: [
      {
        id: 1,
        nombre: 'Allegro Eventos',
        coverPhoto: 'images/1.jpg',
        slug: 'allegro-eventos'
      },
      {
        id: 2,
        nombre: 'Alebrije Eventos Infantiles',
        coverPhoto: 'images/2.jpg',
        slug: 'alebrije-eventos-infantiles'
      },
      {
        id: 3,
        nombre: 'Korima',
        coverPhoto: 'images/3.jpg',
        slug: 'korima'
      },
      {
        id: 4,
        nombre: 'Cucara Macara',
        coverPhoto: 'images/4.jpg',
        slug: 'cucara-macara'
      },
      {
        id: 5,
        nombre: 'Stromboli',
        coverPhoto: 'images/5.jpg',
        slug: 'stromboli'
      },
      {
        id: 6,
        nombre: 'Station Kids',
        coverPhoto: 'images/6.jpg',
        slug: 'station-kids'
      },
      {
        id: 7,
        nombre: 'Fiesta Papalote Valle de Lago',
        coverPhoto: 'images/7.jpg',
        slug: 'fiesta-papalote-valle-de-lago'
      }, {
        id: 8,
        nombre: 'Daleda',
        coverPhoto: 'images/8.jpg',
        slug: 'daleda'
      }, {
        id: 9,
        nombre: 'Fiesta Papalote Progreso',
        coverPhoto: 'images/9.jpg',
        slug: 'fiesta-papalote-progreso'
      },
      {
        id: 10,
        nombre: 'Playroom',
        coverPhoto: 'images/10.jpg',
        slug: 'playroom'
      }
    ]
  })
})

module.exports = app
