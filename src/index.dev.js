const errorHandler = require('errorhandler')
const browserSync = require('browser-sync')
const app = require('./express')
const logger = require('./logger')

app.use(errorHandler())

/**
 * Start Express server.
 */
app.listen(app.get('port'), () => {
  browserSync({
    files: ['src/**/*.{pug,css}'],
    online: false,
    open: false,
    port: 8081,
    proxy: `localhost:${app.get('port')}`,
    ui: false
  })
  logger.info('App is running at http://localhost:%d in %s mode', app.get('port'), app.get('env'))
  logger.info('  Press CTRL-C to stop\n')
})
