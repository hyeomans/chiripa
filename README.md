## Como correr el proyecto

* Tener Node.js instalado
* Instala las dependencias. En una terminal corre: `npm install`
* Luego corre: `npm run dev`
* Abre el browser en: `http://localhost:8081/`