const purgecss = require('@fullhuman/postcss-purgecss')
const nano = require('cssnano')
const plugins = [
  require('tailwindcss')('./src/public/css/tailwind.config.js'),
  require('autoprefixer')
]

if (process.env.NODE_ENV === 'production') {
  plugins.concat([
    purgecss({
      content: ['./src/views/**/*.pug']
    }),
    nano({ preset: 'default' })
  ])
}

module.exports = {
  plugins
}
